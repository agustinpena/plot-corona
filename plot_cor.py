#! /Library/Frameworks/Python.framework/Versions/3.8/bin/python3
# This script plots COVID stats according to
# provided country and stat.


# import needed modules
import requests
from matplotlib import pyplot as plt
import click

# define constants
ENDPOINT = 'http://corona-api.com/countries/'

# define a dictionary
dict_stats = {'c': ['confirmed', 'Days', 'Confirmed'],
              'nc': ['new_confirmed', 'Days', 'New Infected'],
              'd': ['deaths', 'Days', 'Deaths'],
              'nd': ['new_deaths', 'Days', 'New Deaths'],
              'r': ['recovered', 'Days', 'Recovered'],
              'nr': ['new_recovered', 'Days', 'New Recovered'],
              }
# dates = [x['date'] for x in needed_info]


# define a few functions


def plot_graph(option, country, info):
    list_option = [x[dict_stats[option][0]] for x in info]
    plt.plot(list_option)
    plt.title(country)
    plt.xlabel(dict_stats[option][1])
    plt.ylabel(dict_stats[option][2])
    plt.legend([dict_stats[option][2]])
    plt.show()


@click.command()
@click.option('--country', '-c', default='ru', help='''ISO-2 country code.
              Default=ru''')
@click.option('--stat', '-s', default='c', help='''Statistic you want to plot.
                Default = c''')
def main(country, stat):
    """This script collects info about COVID for a requested
    country and plots a requested statistic.

    Available statistics:\n
    c -> confirmed infected\n
    nc -> new confirmed infected\n
    d -> confirmed total deaths\n
    nd -> new confirmed deaths\n
    r -> recovered cases\n
    nr -> new recovered cases"""

    # collect the info through the api
    endpoint = ENDPOINT + country
    response = requests.get(endpoint)
    whole_info = response.json()
    reversed_info = whole_info['data']['timeline']
    needed_info = reversed_info[::-1]
    country_name = whole_info['data']['name']

    # plot the graph
    plot_graph(stat, country_name, needed_info)


if __name__ == '__main__':
    main()
